## Project setup

1. Install dependencies

```bash
npm i
```

2. Run development server

```bash
npm run dev
```

Then open http://localhost:8080
