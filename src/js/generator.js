import { range, shuffle, split } from "./utils";

/**
 * Generate data for square Shulte table
 * @param {Number} n Square side
 * @returns Array n * n
 */
export function generate(n) {
  n = n || 5;

  // Validate square side
  if (n % 2 === 0) {
    throw Error("Square side must be odd");
  }
  const flatSize = Math.pow(n, 2);

  // Generate flat array
  const data = shuffle(range(1, flatSize + 1));

  // Find index of digit "1"
  const indexOf1 = data.indexOf(1);

  // Calculate index of center element
  const center = Math.floor(flatSize / 2);

  // Put "1" to center
  [data[indexOf1], data[center]] = [data[center], data[indexOf1]];

  return split(data, n);
}
