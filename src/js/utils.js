/**
 * Generate range of numbers
 * @param {Number} a Start
 * @param {Number} b End (not included)
 * @param {Number} step Step size
 * @returns Array of numbers
 */
export function range(a, b, step) {
  if (arguments.length === 1) {
    b = a;
    a = 0;
  }
  step = step || 1;

  let x;
  const r = [];
  for (x = a; (b - x) * step > 0; x += step) {
    r.push(x);
  }
  return r;
}

/**
 * Split array into chunks
 * @param {Array} array Source array
 * @param {Number} size Chunk size
 * @returns Array of chunks
 */
export function split(array, size) {
  const result = [];
  for (let i = 0, len = array.length; i < len; i += size) {
    result.push(array.slice(i, i + size));
  }
  return result;
}

/**
 * Shuffle array
 * @param {Array} array Source array
 * @returns Randomized array
 */
export function shuffle(array) {
  let currentIndex = array.length,
    randomIndex;

  while (currentIndex != 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }
  return array;
}
