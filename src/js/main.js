import { generate } from "./generator";
import "../css/main.css";

const table = document.createElement("table");
table.classList.add("shulte");
table.setAttribute("cellspacing", "0");
table.setAttribute("cellpadding", "0");

generate(5).forEach((r) => {
  const row = document.createElement("tr");
  r.forEach((c) => {
    const cell = document.createElement("td");
    cell.innerText = c;
    cell.dataset.value = c;
    row.appendChild(cell);
  });
  table.appendChild(row);
});

document.body.appendChild(table);
